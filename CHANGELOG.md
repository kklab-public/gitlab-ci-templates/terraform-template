## [0.5.7](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.6...v0.5.7) (2024-03-28)


### Bug Fixes

* miss $ suffix on directories_to_targets variable. ([27ef8b0](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/27ef8b012daf0a3601f24d614d8be79215219136))

## [0.5.6](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.5...v0.5.6) (2024-03-22)


### Refactor

* use directories_to_targets variable and '|| true' to avoid error when 'git diff' result is empty. ([d60ab72](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/d60ab725ca0e301fcb89b14673d20c56b3617440))

## [0.5.5](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.4...v0.5.5) (2024-03-22)


### Bug Fixes

* Fix terraform-validate merge to main or master branch job fail error. ([e43dac5](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/e43dac5355a4b766ed1393259e7ee8eb7b8378ff))

## [0.5.4](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.3...v0.5.4) (2024-03-21)


### Bug Fixes

* Replcae gitlab ci only by rules. ([fc6ac01](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/fc6ac01fa2a793f623c472364b4299e868560ca8))

## [0.5.3](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.2...v0.5.3) (2024-03-21)


### Refactor

* Delete git config --list from terraform validate. ([06e0df1](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/06e0df148d6c965b41b367d22c4e3660d2a8e65e))

## [0.5.2](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.1...v0.5.2) (2024-02-23)


### CI/CD

* Upgrade semantic-release nodejs version to 20 ([e39cb60](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/e39cb600593a488ac7ca4c66039114b5b6de03e1))

## [0.5.1](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.5.0...v0.5.1) (2024-01-02)


### Bug Fixes

* xargs with multiple dir issue. ([fa28110](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/fa28110976b92582c754269adcb92fa65a5cd8be))

## [0.5.0](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.4.2...v0.5.0) (2023-12-25)


### Features

* Add tf change dir check to .terraform-validate for reduce validate time. ([30e94a8](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/30e94a860a7a89297ee239b5a7acf0007d256f3f))

## [0.4.2](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.4.1...v0.4.2) (2023-06-09)


### CI/CD

* Update general template to v0.15.0 ([01fa942](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/01fa94246c72e339bb01c4c6a4737141fb7537d9))

## [0.4.1](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.4.0...v0.4.1) (2023-06-08)


### CI/CD

* Update general template to v0.14.1 ([cd01bcf](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/cd01bcf80603f95c8c92e3be501c2d996a936127))
* Update node to 18 in semantic-release ([2572966](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/257296646d7960bbdeb457e522a6ba95adc280d7))

## [0.4.0](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.3.7...v0.4.0) (2022-05-25)


### Features

* Use CI_SERVER_HOST instead fixed hostname ([9cff3b4](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/9cff3b4105dea10e55f595a3b6eeb6462e82817c))

### [0.3.7](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.3.6...v0.3.7) (2022-04-11)


### CI/CD

* update tflint-bundle version to latest. ([5a3c32a](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/5a3c32a1ef162c3d277273f04aeb1e785c37f0da))

### [0.3.6](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.3.5...v0.3.6) (2022-04-08)


### Bug Fixes

* fix tflint-bundle version on v0.34.1.2. ([8f957d9](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/8f957d9c8f373554df1c6b0ad1e8ff540e366e6a))

### [0.3.5](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/compare/v0.3.4...v0.3.5) (2022-01-27)


### CI/CD

* Add editorconfig-lint ([28f8b6d](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/28f8b6d695a6ecbc8e6c941f84cc378117041d49))
* Add jsonlint ([86e66c8](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/86e66c8eb62d7d918f8f5d45680ac7486829e2cf))
* Add semantic-release ([03ad54e](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/03ad54e2baf8a80fee6f34da5cca6aa1a0cf1f39))
* Update yamllint template version ([3c6ad34](https://gitlab.com/kklab-public/gitlab-ci-templates/terraform-template/commit/3c6ad3481b154afe7ddb102452a87102198b29e3))

# Change log

## v0.1.0

Features:

- Add [.managed-tag-check](test/managed-tag-check.yml)
- Add [.terraform-fmt](test/terraform-fmt.yml)
- Add [.terraform-validate](test/terraform-validate.yml)
- Add [.tflint](test/tflint.yml)

## v0.2.0

Updates:

- Remove runner tag [.terraform-fmt](test/terraform-fmt.yml)
- Update base image version [.tflint](test/tflint.yml)
- Remove .managed-tag-check

Features:

- Add [.managed-tag-check-aws](test/managed-tag-check-aws.yml)
- Add [.managed-tag-check-azure](test/managed-tag-check-azure.yml)

## v0.2.1

Updates:

- Fix naming error [.managed-tag-check-azure](test/managed-tag-check-azure.yml)

## v0.2.2

Updates:

- Add ssh setting to CI terraform-validate section [.terraform-validate](test/terraform-validate.yml)

## v0.2.3

Updates:

- Allow lowercase tag on aws resource [.managed-tag-check-aws](test/managed-tag-check-aws.yml)

## v0.3.0

Updates:

- Migrate to gitlab.com
- Use gitlab ci job token to clone terraform module [.terraform-validate](test/terraform-validate.yml)

## v0.3.1

Updates:

- Specific match resource tags [.managed-tag-check-aws](test/managed-tag-check-aws.yml)

## v0.3.2

Updates:

- echo which project is tested [.terraform-validate](test/terraform-validate.yml)

## v0.3.3

Updates:

- use tflint official bundle image [.tflint](test/tflint.yml)

## v0.3.4

Updates:

- add tflint init [.tflint](test/tflint.yml)
